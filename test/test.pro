TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

SOURCES += \
    $$PWD/../src/game.cpp \
    $$PWD/../src/field.cpp

HEADERS += \
    $$PWD/../src/game.h \
    $$PWD/../src/field.h \

INCLUDEPATH += $$PWD/../third_party/inc\
               $$PWD/../src


LIBS += -lgtest_main -lgtest
LIBS += -L"$$PWD/../third_party/lib/"

