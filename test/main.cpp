#include "game.h"
#include "gtest/gtest.h"

namespace {
TEST(Game, make_move_test_1) {
  Game g(6,7,4,Game::BLUE);
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(1));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(1));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(1));
  EXPECT_EQ(Game::BLUE_WON, g.make_move(0));
}
TEST(Game, make_move_test_2) {
  Game g(4,4,3,Game::BLUE);
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::RE_MOVE, g.make_move(0));
}
TEST(Game, make_move_test_3) {
  Game g(7,6,2,Game::RED);
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::RED_WON, g.make_move(1));
}
TEST(Game, make_move_test_4) {
  Game g(20,20,3,Game::RED);
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(1));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(1));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(2));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(10));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(2));
  EXPECT_EQ(Game::RED_WON, g.make_move(2));
}
TEST(Game, make_move_test_5) {
  Game g(3,2,10,Game::BLUE);
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(1));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(0));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(1));
  EXPECT_EQ(Game::NEXT_MOVE, g.make_move(2));
  EXPECT_EQ(Game::DRAW, g.make_move(2));
}
}
