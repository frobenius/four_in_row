#-------------------------------------------------
#
# Project created by QtCreator 2017-10-13T21:16:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Four_in_a_row
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
       src/main.cpp \
       src/mainwindow.cpp \
    src/dialog.cpp \
    src/game.cpp \
    src/field.cpp

HEADERS += \
        src/mainwindow.h \
    src/dialog.h \
    src/game.h \
    src/field.h \
    src/gameviewer.h

FORMS += \
        src/mainwindow.ui \
    src/dialog.ui

CONFIG(debug, debug|release) {
    DESTDIR = $$PWD/debug/
    OUTDIR = $$PWD/debug/
    OBJECTS_DIR = $$PWD/debug/.obj
    MOC_DIR = $$PWD/debug/.moc
} else {
    DESTDIR = $$PWD/release/
    OUTDIR = $$PWD/release/
    OBJECTS_DIR = $$PWD/release/.obj
    MOC_DIR = $$PWD/release/.moc
}

