#ifndef GAMEVIEWER_H
#define GAMEVIEWER_H

#include "game.h"
#include <cassert>
#include <algorithm>
#include <QWidget>
#include <QPainter>

class GameViewer
{
public:
    static void show_game_on_widget(Game * g, QWidget * widget)
    {
        QPainter painter(widget);
        assert(g->get_field_width() > 0 && g->get_field_height() > 0);
        float dx = float(widget->width()) / g->get_field_width();
        const int menu_margin = 50;
        float dy = float((widget->height() - menu_margin)) / g->get_field_height();

        painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::SquareCap));
        for (int column = 1; column < g->get_field_width(); ++column)
            painter.drawLine(column * dx , menu_margin, column * dx, widget->height());

        painter.setPen(QPen(Qt::black, 0));
        for (int row = 0; row < g->get_field_height(); ++row)
            for (int column = 0; column < g->get_field_width(); ++column)
            {
                int coin = g->get_coin_at_pos(column, row);
                if (coin != -1)
                {
                      painter.setBrush((coin) ? Qt::red : Qt::blue);
                      painter.drawEllipse(QPointF(float(column) * dx + dx / 2.0f, float(widget->height()) - float(row) * dy - 0.5f * dy),
                                          std::min(dx / 2.0f, dy / 2.0f), std::min(dx / 2.0f, dy / 2.0f));
                }
            }
    }
private:
    GameViewer();
    ~GameViewer();
    GameViewer(const GameViewer &);
    GameViewer & operator=(const GameViewer &);
};

#endif // GAMEVIEWER_H
