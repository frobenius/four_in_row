#include <cassert>
#include "game.h"

Game::Game(int n, int m, int win_threshold, PLAYER turn) : max_moves(n * m), m_turn(turn), m_move_count(0)
{
    assert(n < 100 && n > 0 && m < 100 && m > 0 && turn >= 0 && turn < 2);
    f = new Field(n, m, win_threshold);
}

Game::~Game()
{
    delete f;
}

Game::GAME_STATUS Game::make_move(const int column)
{
    int result = f->coin_throw(column, m_turn);
    switch (result)
    {
        case (-1): return RE_MOVE;
                   break;
        case (0):  ++m_move_count;
                   m_turn = (m_turn + 1) % 2;
                   return (m_move_count == max_moves) ? DRAW : NEXT_MOVE;
                   break;
        case (1):  return (m_turn) ? RED_WON : BLUE_WON;
                   break;
        default:{};
    }
    return GAME_CRASH;
}

int Game::get_coin_at_pos(const int column, const int row) const
{
    return f->get_coin_at_pos(column, row);
}

int Game::get_turn() const
{
    return m_turn;
}

int Game::move_count() const
{
    return m_move_count;
}

int Game::get_field_width() const
{
    return f->get_width();
}

int Game::get_field_height() const
{
    return f->get_height();
}
