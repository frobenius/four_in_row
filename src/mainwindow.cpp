#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cassert>
#include <stdlib.h>
#include <time.h>
#include <QPainter>
#include <QMouseEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    g(NULL)
{
    ui->setupUi(this);
    srand (time(NULL));
    create_new_game();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete g;
}

void MainWindow::on_EndGame(Game::GAME_STATUS status)
{
    dialog = new Dialog(this, status);
    dialog->setAttribute(Qt::WA_DeleteOnClose); // this will automatically delete dialog window after on close(), preventing memory leak
    QObject::connect(dialog, SIGNAL(finished(int)), this, SLOT(create_new_game()));
    dialog->show();
}

void MainWindow::create_new_game()
{
    if (g)
        delete g;
    g = new Game(6, 7, 4, rand() % 2 ? Game::BLUE : Game::RED);   //tested for up to 99 by 99 fields
    update();
}

void MainWindow::paintEvent(QPaintEvent *)
{
    GameViewer::show_game_on_widget(g, this);
}

void MainWindow::mouseReleaseEvent(QMouseEvent * event)
{
    if (event->button() == Qt::LeftButton)
    {
        assert(g->get_field_width() > 0 && width() > 0);
        float dx = float(width()) / g->get_field_width();
        assert(dx > 0.0f);
        int column = int(float(event->x()) / dx);
        Game::GAME_STATUS status = g->make_move(column);
        switch (status)
        {
            case (Game::RE_MOVE):
            case (Game::NEXT_MOVE): break;
            default:{on_EndGame(status);}
        }
        update();
    }
}

void MainWindow::on_actionExit_triggered()
{
    close();
}

void MainWindow::on_actionRestart_triggered()
{
    create_new_game();
}
