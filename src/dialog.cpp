#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent, Game::GAME_STATUS status) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    switch (status)
    {
         case (Game::BLUE_WON): ui->label->setText("<font color='blue'>BLUE</font> <font color='black'>WON!</font>");
                                break;
         case (Game::RED_WON):  ui->label->setText("<font color='red'>RED</font> <font color='black'>WON!</font>");
                                break;
         case (Game::DRAW):     ui->label->setText("<font color='red'>DR</font><font color='blue'>AW</font>");
                                break;
         default:{ui->label->setText("Something went terribly wrong. Sorry.");};
    }
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_pressed()
{
    close();
}
