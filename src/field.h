#ifndef FIELD_H
#define FIELD_H

class Field
{
public:
    Field(int w, int h, int win_threshold);
    ~Field();
    int coin_throw(const int column, const int player);
    int get_coin_at_pos(const int column, const int row) const;
    int get_width() const;
    int get_height() const;

private:
     int check_win_helper(int column, int row, const int dx, const int dy, const int player) const;
     int check_win(const int column, const int row, const int player) const;

     int * m_arr;
     const int m_width;
     const int m_height;
     const int m_win_threshold;
};

#endif // FIELD_H
