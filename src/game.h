#ifndef GAME_H
#define GAME_H

#include "field.h"

class Game
{
public:
    enum GAME_STATUS {GAME_CRASH, RE_MOVE, NEXT_MOVE, RED_WON, BLUE_WON, DRAW};
    enum PLAYER {BLUE, RED};
    Game(int n, int m, int win_threshold, PLAYER turn);
    ~Game();
    GAME_STATUS make_move(const int column);
    int get_turn() const;
    int move_count() const;
    int get_field_width() const;
    int get_field_height() const;
    int get_coin_at_pos(const int column, const int row)const;
private:
    const int max_moves;
    int m_turn;
    int m_move_count;
    Field * f;
};

#endif // GAME_H
