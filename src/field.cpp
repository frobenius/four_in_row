#include "field.h"
#include <algorithm>

Field::Field(int w, int h, int win_threshold) : m_width(w), m_height(h), m_win_threshold(win_threshold)
{
    m_arr = new int[w * h];
    std::fill(m_arr, m_arr + w * h, -1);
}

Field::~Field()
{
    delete[] m_arr;
}

int Field::check_win_helper(int column, int row, const int dx, const int dy, const int player) const
{
      column += dx;
      row += dy;
      int count = 0;

      while((column >= 0) && (column < m_width) &&
            (row >= 0)    && (row < m_height))
      {
         if (m_arr[column * m_height + row] == player)
         {
             ++count;
             column += dx;
             row += dy;
         }
         else
             break;
     }
     return count;
}

int Field::check_win(const int column, const int row, const int player) const
{
    bool horizontal =
            (1 + check_win_helper(column, row, 1, 0, player) + check_win_helper(column, row, -1, 0, player)) >= m_win_threshold;
    bool vertical =
            (1 + check_win_helper(column, row, 0, 1, player) + check_win_helper(column, row, 0, -1, player)) >= m_win_threshold;
    bool main_diag =
            (1 + check_win_helper(column, row, 1, -1, player) + check_win_helper(column, row, -1, 1, player)) >= m_win_threshold;
    bool aux_diag =
            (1 + check_win_helper(column, row, 1, 1, player) + check_win_helper(column, row, -1, -1, player)) >= m_win_threshold;

    return (horizontal || vertical || main_diag || aux_diag);
}

int Field::coin_throw(const int column, const int player)
{
    for (int h = 0; h < m_height; ++h)
    {
        if(m_arr[column * m_height + h] == -1)
           {
                m_arr[column * m_height + h] = player;
                return check_win(column, h, player);
           };
    }
    return -1;
}

int Field::get_coin_at_pos(const int column, const int row) const
{
    return m_arr[column * m_height + row];
}

int Field::get_width() const
{
    return m_width;
}

int Field::get_height() const
{
    return m_height;
}
