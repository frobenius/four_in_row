#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "game.h"
#include "dialog.h"
#include "gameviewer.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_EndGame(Game::GAME_STATUS status);
    void on_actionExit_triggered();
    void mouseReleaseEvent(QMouseEvent * event);
    void paintEvent(QPaintEvent *);
    void create_new_game();
    void on_actionRestart_triggered();

private:
    Ui::MainWindow * ui;
    Dialog * dialog;
    Game * g;
};

#endif // MAINWINDOW_H
