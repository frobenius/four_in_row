#ifndef DIALOG_H
#define DIALOG_H

#include "game.h"
#include <QDialog>
#include <QLabel>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent, Game::GAME_STATUS status);
    ~Dialog();

private slots:
    void on_pushButton_pressed();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
