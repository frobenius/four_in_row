I've implemented a rudimentary MVC paradigm, except there's not match to control (hence there's no actual controller class) and the viewing does hardly require any data (or rendering functional) 
to be stored (hence viewing function is static). I've not used defensive programming (many types are 'wider' than actually required: ints instead of chars, etc), although I've used 'accerts' to 
provide at least some protection in debug mode. Game and Field classes may be merged but I separated them to avoid monster-class antipattern, giving administrative responsibilities to the Game class and 
'computational' - to the Field class. I've implemented extra functional for the Game class (arbitrary dimensions of the field) partly for the testing purposes.
I also deliberately avoided using C++0x features (nullptrs, smart pointers etc). I checked the program builds on Windows 10 and Ubuntu 14. I've used Qt 5.9.2 MinGW 32 bit with Qt Creator 4.4.1.
I've used Dr.Memory to check for possible memory leaks. I've also used a googletest library (third_party) for simple tests.
